import * as Yup from "yup";

const validationSchema = Yup.object({
  name: Yup.string()
    .required("required")
    .typeError("required")
    .max(30, "max symbol is 30")
    .min(2, " min symbol is 2"),

  location: Yup.string()
    .required("required")
    .typeError("required")
    .max(50, "max symbol is 50")
    .min(5, " min symbol is 5"),

  email: Yup.string()
    .required("required")
    .typeError("required")
    .max(50, "max symbol is 50")
    .min(10, " min symbol is 10"),

  image: Yup.string().required("required").typeError("required"),
});

export default validationSchema;
