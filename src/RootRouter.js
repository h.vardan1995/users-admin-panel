import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Users, UsersAddEdit } from "./components/users";

const RootRouter = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Users />
        </Route>
        <Route exact path="/user/add">
          <UsersAddEdit />
        </Route>
        <Route exact path="/user/edit/:id">
          <UsersAddEdit />
        </Route>
        <Redirect to="/" />
      </Switch>
    </div>
  );
};

export default RootRouter;
