import React from "react";

import { BrowserRouter as Router } from "react-router-dom";
import RootRouter from "./RootRouter";
import { Header, SideBar } from "./components/navigations";
import Styles from "./App.module.scss";
function App() {
  return (
    <Router>
      <Header />
      <div className={Styles.mainAppWrapper}>
        <SideBar />
        <RootRouter />
      </div>
    </Router>
  );
}

export default App;
