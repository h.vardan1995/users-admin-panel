import { createStore, applyMiddleware, compose } from "redux";
import {routerMiddleware} from 'connected-react-router';
import thunk from "redux-thunk";
import logger from "redux-logger";
import rootReducer, {history} from './rootReducer';

let middleware = [thunk, logger, routerMiddleware(history)];
const store = createStore(rootReducer, compose(applyMiddleware(...middleware)));

export default store;
