import { connectRouter } from "connected-react-router";
import { createBrowserHistory } from "history";
import { combineReducers } from "redux";

//reducers
import usersReducer from "./users/index";

export const history = createBrowserHistory();

export default combineReducers({
  router: connectRouter(history),
  users: usersReducer,
});
