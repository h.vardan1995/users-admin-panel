export const CHANGE_LOADER = "CHANGE_LOADER";
export const CHANGE_OPACITY_LOADER = "CHANGE_OPACITY_LOADER";

export const changeLoader = (bool) => ({
  type: CHANGE_LOADER,
  payload: bool,
});

export const changeOpacityLoader = (value) => ({
  type: CHANGE_OPACITY_LOADER,
  payload: value,
});

const initialState = {
  loading: false,
  error: false,
  opacityLoader: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_LOADER:
      return {
        ...state,
        loading: action.payload,
      };
    case CHANGE_OPACITY_LOADER:
      return {
        ...state,
        opacityLoader: action.payload,
      };
    default:
      return state;
  }
};
