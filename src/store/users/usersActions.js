import axios from "axios";
import { GET_USERS, GET_ERROR, GET_USER } from "./usersActionTypes";
import { changeLoader } from "../state/index";
import cogoToast from "cogo-toast";
// import { push } from "connected-react-router";

export const actionCreators = {
  getUsers: (users) => ({
    type: GET_USERS,
    payload: users,
  }),

  getUser: (user) => ({
    type: GET_USER,
    payload: user,
  }),

  getError: (error) => ({
    type: GET_ERROR,
    payload: error,
  }),
};

export const getAllUsers = (page, pageSize) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    const data = await axios.get(
      `https://brainstorm-interview-task.herokuapp.com/users?_page=${page}&_limit=${pageSize}`
    );
    dispatch(actionCreators.getUsers(data));
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    dispatch(changeLoader(false));
  }
};

export const getUser = (userId) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    const { data } = await axios.get(
      `https://brainstorm-interview-task.herokuapp.com/users/${userId}`
    );
    dispatch(actionCreators.getUser(data));
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    dispatch(changeLoader(false));
  }
};

export const createUser = (data) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    await axios.post(
      "https://brainstorm-interview-task.herokuapp.com/users",
      data
    );
    cogoToast.success("User was added");
    // dispatch(push(`/user`));
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    cogoToast.error("Something went wrong !!! ");
    dispatch(changeLoader(false));
  }
};

export const updateUser = (id, data) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    await axios.patch(
      `https://brainstorm-interview-task.herokuapp.com/users/${id}`,
      data
    );
    cogoToast.success("User was edited");
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    cogoToast.error("Something went wrong !!! ");
    dispatch(changeLoader(false));
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    await axios.delete(
      `https://brainstorm-interview-task.herokuapp.com/users/${id}`
    );
    cogoToast.success("User was deleted");
    dispatch(getAllUsers());
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    cogoToast.error("Something went wrong !!! ");
    dispatch(changeLoader(false));
  }
};

export const sort = (sortName, sortBy) => async (dispatch) => {
  try {
    dispatch(changeLoader(true));
    await axios.get(
      `https://brainstorm-interview-task.herokuapp.com/users/?_sort=${sortName}&_order=${sortBy}`
    );
    cogoToast.success("Sorted");
    dispatch(getAllUsers());
    dispatch(changeLoader(false));
  } catch (error) {
    dispatch(actionCreators.getError(error));
    cogoToast.error("Something went wrong !!! ");
    dispatch(changeLoader(false));
  }
};
