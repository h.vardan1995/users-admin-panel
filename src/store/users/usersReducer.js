import {
  GET_USERS,
  GET_ERROR,
  GET_USER,
  CREATE_USER,
  SET_EDIT_USER_INFO,
  DELETE_USER,
} from "./usersActionTypes";

const initialState = {
  users: null,
  user: null,
  loading: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case GET_USER:
      return {
        ...state,
        user: action.payload,
      };

    case GET_ERROR:
      return {
        ...state,
        error: action.payload,
      };

    default:
      return state;
  }
};
