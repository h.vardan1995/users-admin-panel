import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import classNames from "classnames";
import moment from "moment";
import { Switch } from "@material-ui/core";

import { deleteUser, getAllUsers } from "../../store/users/usersActions";
import {
  CustomTableHead,
  CustomTableBody,
  CustomPagination,
  CustomDialog,
  CutomPageTitle,
} from "../shared";

import Styles from "./users.module.scss";

const Users = () => {
  const dispatch = useDispatch();
  const [selected, setSelected] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [isOpenDelete, setIsOpenDelete] = useState(false);
  const [id, setId] = useState("");

  const users = useSelector((state) => state.users?.users?.data);
  const total = useSelector(
    (state) => state.users?.users?.headers?.["x-total-count"]
  );

  useEffect(() => {
    dispatch(getAllUsers(selected, pageSize));
  }, [selected, pageSize]);

  return (
    <>
      <div
        className={classNames("ml-5 mr-5", {
          [Styles.UserTableWrapper]: !isOpenDelete,
          [Styles.UsersTableWrapperBlur]: isOpenDelete,
        })}
      >
        <CutomPageTitle title="All Users" />
        {users ? (
          <table className="table table-striped">
            <CustomTableHead
              columns={[
                { key: "photo", title: "Photo" },
                { key: "name", title: "Name" },
                { key: "location", title: "Location" },
                { key: "registeredDate", title: "RegisteredDate" },
                { key: "lastActiveDate", title: "LastActiveDate" },
                { key: "email", title: "Email" },
                { key: "action", title: "Actions" },
              ]}
            />

            <CustomTableBody
              tableColumns={users}
              renderColumns={(column) => (
                <>
                  <td>
                    {column.photo ? (
                      <img
                        className={Styles.UserAvatarImg}
                        src={column.photo}
                        alt="img"
                      />
                    ) : (
                      "-"
                    )}
                  </td>
                  <td>
                    {column.name ? (
                      <Link to={`/user/edit/${column.id}`}>{column.name}</Link>
                    ) : (
                      "-"
                    )}
                  </td>
                  <td>{column.location ? column.location : "-"}</td>
                  <td>
                    {column.registeredDate
                      ? moment(column.registeredDate).format("DD.mm.yyyy")
                      : "-"}
                  </td>
                  <td>
                    {column.lastActiveDate
                      ? moment(column.lastActiveDate).format("DD.mm.yyyy")
                      : "-"}
                  </td>
                  <td>
                    <i class="fas fa-envelope"></i>
                  </td>
                  <td>
                    <Switch
                      defaultChecked={column.disabled}
                      size="small"
                      color="secondary"
                    />

                    <span className="ml-3">
                      <i
                        onClick={() => {
                          setIsOpenDelete(!isOpenDelete);
                          setId(column.id);
                        }}
                        className={classNames(
                          "fas fa-trash-alt",
                          Styles.CursorColored
                        )}
                      ></i>
                    </span>
                  </td>
                </>
              )}
            />
          </table>
        ) : (
          "loading..."
        )}
        <div className={Styles.PaginationSection}>
          <CustomPagination
            count={Math.ceil(+total / pageSize)}
            value={selected}
            onChange={(value) => setSelected(value)}
          />

          <select
            onChange={(e) => {
              setPageSize(e.target.value);
            }}
          >
            <option value="5">5 / page</option>
            <option value="10" selected>
              10 / page
            </option>
          </select>
          <div className={Styles.Total}>
            Total number of users:<span> {total} </span>
          </div>
        </div>
      </div>
      <div className={Styles.PopupWrapper}>
        {isOpenDelete && (
          <CustomDialog
            title="Delete ?"
            deleteAction={() => {
              dispatch(deleteUser(+id));
              setIsOpenDelete(!isOpenDelete);
            }}
            cancelAction={() => {
              setIsOpenDelete(!isOpenDelete);
            }}
          />
        )}
      </div>
    </>
  );
};

export default Users;
