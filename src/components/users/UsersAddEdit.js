import classNames from "classnames";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";

import { convertFileToBase64 } from "../../helpers/converFileToBase64";
import {
  createUser,
  getUser,
  updateUser,
} from "../../store/users/usersActions";
import { validationSchema } from "../../validations";
import { AppForm, AppFormField } from "../forms";
import { CustomButton, CutomPageTitle } from "../shared";

import Styles from "./users.module.scss";

const UsersAddEdit = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.users?.user);

  const [postImage, setPostImage] = useState({
    myFile: "",
  });
  const { id } = useParams();

  const handleFileUpload = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertFileToBase64(file);
    setPostImage({ ...postImage, myFile: base64 });
  };
  const image64 = postImage.myFile;

  useEffect(() => {
    if (id) dispatch(getUser(id));
  }, []);
  if (!user && id) return null;
  return (
    <div className="ml-5 mr-5 mt-3 ">
      <CutomPageTitle
        title={!id ? "New user" : "Edit user"}
        showButton={false}
        className="mb-3"
      />
      <div className={classNames("w-100", Styles.ActionForm)}>
        <div className="w-50 pr-4 pl-4 pb-4 ">
          <AppForm
            initialValues={{
              id: id && user.id,
              name: id && user.name,
              email: id && user.email,
              location: id && user.location,
              image: id && user.image,
            }}
            onSubmit={(values) => {
              if (id) {
                dispatch(
                  updateUser(+id, {
                    id,
                    name: values.name,
                    email: values.email,
                    location: values.location,
                    image: image64,
                  })
                );
              } else
                dispatch(
                  createUser({
                    id: values.id,
                    name: values.name,
                    email: values.email,
                    location: values.location,
                    image: image64,
                  })
                );
            }}
            validationSchema={validationSchema}
          >
            {(field) => {
              console.log(field);
              return (
                <>
                  <div>
                    <div>
                      <AppFormField placeholder="User name" name="name" />
                      <AppFormField placeholder="Email" name="email" />
                      <AppFormField placeholder="Location" name="location" />
                      <AppFormField
                        type="file"
                        name="image"
                        onChange={(e) => {
                          handleFileUpload(e);
                        }}
                      />
                    </div>

                    <CustomButton title="Save " />
                  </div>
                </>
              );
            }}
          </AppForm>
        </div>
      </div>
    </div>
  );
};

export default UsersAddEdit;
