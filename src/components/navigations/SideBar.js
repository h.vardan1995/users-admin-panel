import classNames from "classnames";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import Styles from "./navigation.module.scss";

const SideBar = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  const handleOnClick = (index) => {
    setActiveIndex(index);
  };
  const boxs = [
    { icon: "fas fa-home", name: "HomePage", navigation: "/" },
    { icon: "far fa-user", name: "Users", navigation: "/" },
    { icon: "far fa-user", name: "Premium users", navigation: "/" },
    { icon: "fas fa-plus", name: "ADD user", navigation: "/user/add" },
  ];

  const box = boxs.map((el, index) => {
    return (
      <div
        key={index}
        onClick={() => handleOnClick(index)}
        className={activeIndex === index ? Styles.Active : ""}
      >
        <Link to={el.navigation}>
          <div className={classNames("m-3", Styles.Colors)}>
            <i className={el.icon}></i>
            <span className="ml-3">{el.name}</span>
          </div>
        </Link>
      </div>
    );
  });

  return (
    <div className={Styles.MainSideBarWrapper}>
      <div className={Styles.Items}>{box}</div>
    </div>
  );
};

export default React.memo(SideBar);
