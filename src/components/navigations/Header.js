import classNames from "classnames";
import React from "react";
import Styles from "./navigation.module.scss";

const Header = () => {
  return (
    <div className={Styles.MainHeaderWrapper}>
      <div className={Styles.FirstPart}></div>
      <div className={Styles.HeaderItems}>
        <i class="fas fa-search mr-3"></i>
        <i class="fas fa-bell mr-5"></i>
        <div className={classNames(Styles.UserWrapper, "mr-3")}>
          <div className={Styles.IsOnline}></div>
        </div>
      </div>
    </div>
  );
};

export default React.memo(Header);
