import React from "react";
import { Formik, Form } from "formik";

const AppForm = ({ initialValues, onSubmit, validationSchema, children }) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {(...props) => (
        <Form noValidate enctype="multipart/form-data">
          {children(...props)}
        </Form>
      )}
    </Formik>
  );
};

export default React.memo(AppForm);
