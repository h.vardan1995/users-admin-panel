import React from "react";
import { ErrorMessage, Field } from "formik";

import { CustomInput } from "../shared";
import { default as CustomErrorMessage } from "./ErrorMessage";

const AppFormField = ({ name, ...otherProps }) => {
  console.log(otherProps);
  return (
    <>
      <Field name={name}>
        {({ field, _, meta }) => {
          return (
            <>
              <CustomInput
                {...field}
                {...otherProps}
                onChange={(...args) => {
                  field.onChange(...args);
                  if (otherProps.onChange) otherProps.onChange(...args);
                }}
                error={meta.touched && meta.error}
              />
              <ErrorMessage name={name} component={CustomErrorMessage} />
            </>
          );
        }}
      </Field>
    </>
  );
};

export default React.memo(AppFormField);
