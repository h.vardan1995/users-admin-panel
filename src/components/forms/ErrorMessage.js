import React from "react";

import styles from "./form.module.scss"

const ErrorMessage = ({ children }) => {
  if (typeof children !== "string") return null;

  return <p className={styles.Error}>{children}</p>;
};

export default React.memo(ErrorMessage);
