import React from "react";

const CustomDialog = ({
  title,
  deleteAction = () => {},
  cancelAction = () => {},
}) => {
  return (
    <div className="p-4">
      {title}

      <div className="mt-4">
        <button
          type="button"
          className="btn btn-danger mr-4"
          onClick={deleteAction}
        >
          Delete
        </button>
        <button
          type="button"
          className="btn btn-primary"
          onClick={cancelAction}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default React.memo(CustomDialog);
