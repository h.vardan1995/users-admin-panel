import React from "react";

const CustomButton = ({ title, ...props }) => {
  return (
    <div>
      <button type="submit" className="btn btn-primary mt-4" {...props}>
        {title}
      </button>
    </div>
  );
};

export default React.memo(CustomButton);
