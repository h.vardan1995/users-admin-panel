import React from "react";

const CustomTableBody = ({ tableColumns, renderColumns = () => {} }) => {
  return (
    <tbody>
      {tableColumns?.map((columns, index) => (
        <tr key={index} className="table-items">
          {renderColumns(columns)}
        </tr>
      ))}
    </tbody>
  );
};

export default React.memo(CustomTableBody);
