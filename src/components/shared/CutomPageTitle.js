import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";

import Styles from "../users/users.module.scss";

const CutomPageTitle = ({ showButton = true, title, className }) => {
  return (
    <div>
      <div className={classNames(Styles.ActionWrapper, className)}>
        <div>{title}</div>
        <div className={Styles.Line}></div>
        {showButton && (
          <Link to="/user/add">
            <button className="btn btn-info mt-2 mb-2 ml-2" type="button">
              New user
            </button>
          </Link>
        )}
      </div>
    </div>
  );
};

export default CutomPageTitle;
