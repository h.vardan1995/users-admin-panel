import React from "react";
import { Form } from "react-bootstrap";

const CustomInput = ({ ...otherProps }) => {
  return (
    <div className="mt-4">
      <Form.Group>
        <Form.Control {...otherProps} />
      </Form.Group>
    </div>
  );
};

export default React.memo(CustomInput);
