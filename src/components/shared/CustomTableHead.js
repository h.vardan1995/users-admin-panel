import React from "react";
import { useDispatch } from "react-redux";

import { sort } from "../../store/users/usersActions";

const CustomTableHead = ({ columns = [] }) => {
  const dispatch = useDispatch();
  const items = columns.map(({ key, title }) => {
    return (
      <th
        style={{ borderTop: "none" }}
        key={key}
        onClick={() => {
          dispatch(sort(key, "desc"));
        }}
      >
        {title}
      </th>
    );
  });
  return (
    <thead>
      <tr style={{ color: "#878FA1" }}>{items}</tr>
    </thead>
  );
};

export default React.memo(CustomTableHead);
